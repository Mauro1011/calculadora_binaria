package com.example.calculadora_binaria;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private CheckBox check0;
    private CheckBox check1;
    private CheckBox check2;
    private CheckBox check3;
    private CheckBox check4;
    private CheckBox check5;
    private CheckBox check6;
    private CheckBox check7;
    private CheckBox check8;
    private CheckBox check9;
    private TextView txtResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.check0=findViewById(R.id.check0);
        this.check1=findViewById(R.id.check1);
        this.check2=findViewById(R.id.check2);
        this.check3=findViewById(R.id.check3);
        this.check4=findViewById(R.id.check4);
        this.check5=findViewById(R.id.check5);
        this.check6=findViewById(R.id.check6);
        this.check7=findViewById(R.id.check7);
        this.check8=findViewById(R.id.check8);
        this.check9=findViewById(R.id.check9);
        this.txtResultado=findViewById(R.id.txtResultado);

        check0.setOnCheckedChangeListener(this);
        check1.setOnCheckedChangeListener(this);
        check2.setOnCheckedChangeListener(this);
        check3.setOnCheckedChangeListener(this);
        check4.setOnCheckedChangeListener(this);
        check5.setOnCheckedChangeListener(this);
        check6.setOnCheckedChangeListener(this);
        check7.setOnCheckedChangeListener(this);
        check8.setOnCheckedChangeListener(this);
        check9.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int var=0,suma=0;
        String res;

        var += (check0.isChecked()) ? 1 : 0;
        var += (check1.isChecked()) ? 2 : 0;
        var += (check2.isChecked()) ? 4 : 0;
        var += (check3.isChecked()) ? 8: 0;
        var += (check4.isChecked()) ? 16 : 0;
        var += (check5.isChecked()) ? 32 : 0;
        var += (check6.isChecked()) ? 64 : 0;
        var += (check7.isChecked()) ? 128 : 0;
        var += (check8.isChecked()) ? 256 : 0;
        var += (check9.isChecked()) ? 512 : 0;

        suma=suma+var;
        res=String.valueOf(suma);
        txtResultado.setText(res);
    }
}
